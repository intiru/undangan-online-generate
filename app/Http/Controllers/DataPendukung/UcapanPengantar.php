<?php

namespace app\Http\Controllers\DataPendukung;

use app\Models\mUcapanPengantar;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;

class UcapanPengantar extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_pendukung'],
                'route' => ''
            ],
            [
                'label' => $cons['ucapan_pengantar'],
                'route' => ''
            ]
        ];
    }

    function index()
    {
        $data = Main::data($this->breadcrumb);
        $data_list = mUcapanPengantar
            ::orderBy('ucp_judul', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('dataPendukung/ucapanPengantar/ucapanPengantarList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'ucp_judul' => 'required',
            'ucp_isi' => 'required',
        ]);

        $data_insert = $request->except('_token');

        mUcapanPengantar::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mUcapanPengantar::where('id_ucapan_pengantar', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataPendukung/ucapanPengantar/ucapanPengantarEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mUcapanPengantar::where('id_ucapan_pengantar', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'ucp_judul' => 'required',
            'ucp_isi' => 'required',
        ]);
        $data = $request->except("_token");

        mUcapanPengantar::where(['id_ucapan_pengantar' => $id])->update($data);
    }
}
