<?php

namespace app\Http\Controllers\Undangan;

use app\Models\mMantraPenutup;
use app\Models\mOrder;
use app\Models\mOrderTamuUndangan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Front extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [];
    }

    function index()
    {
        return 'Halaman Landing Page';
    }


}
