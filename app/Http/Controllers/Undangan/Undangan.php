<?php

namespace app\Http\Controllers\Undangan;

use app\Models\mBacksound;
use app\Models\mMantraPenutup;
use app\Models\mOrder;
use app\Models\mOrderBukuTamu;
use app\Models\mOrderGaleriAcara;
use app\Models\mOrderGaleriCover;
use app\Models\mOrderLokasiWaktu;
use app\Models\mOrderMempelai;
use app\Models\mOrderMempelaiKategori;
use app\Models\mOrderTamuUndangan;
use app\Models\mOrderVideo;
use app\Models\mUcapanPengantar;
use app\Models\mUcapanPenutup;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

class Undangan extends Controller
{
    private $breadcrumb;

    function __construct()
    {

    }

    function index($ord_subdomain, $id_order_tamu_undangan = '')
    {
        $check = mOrder::where('ord_subdomain', $ord_subdomain)->count();
        if ($check == 0) {
            return redirect()->route('frontHome', ['undangan' => 'none']);
        }

        $id_order_tamu_undangan = Main::decrypt_order($id_order_tamu_undangan);
        $order = mOrder::where('ord_subdomain', $ord_subdomain)->first();

        $backsound = mBacksound::where('id_backsound', $order->id_backsound);
        if ($backsound->count() > 0) {
            $backsound = $backsound->first();
        } else {
            $backsound = [
                'bks_filename' => ''
            ];
        }

        $cover = mOrderGaleriCover::where('id_order', $order->id_order);
        if($cover->count() > 0) {
            $cover = $cover->get();
        } else {
            $cover = [];
        }

        $lokasi_waktu = mOrderLokasiWaktu::where('id_order', $order->id_order);
        if($lokasi_waktu->count() > 0) {
            $lokasi_waktu = $lokasi_waktu->first();
        } else {
            $lokasi_waktu = [
                'olw_tanggal'=> date('Y-m-d'),
                'olw_alamat' => '',
                'olw_map_url' => '',
                'olw_jam_mulai' => date('H:i:s')
            ];
        }

        $mempelai = mOrderMempelai::where('id_order', $order->id_order);
        if($mempelai->count() > 0) {
            $mempelai = $mempelai->get();
        } else {
            $mempelai = [];
        }

        $pengantar = mUcapanPengantar::where('id_ucapan_pengantar', $order->id_ucapan_pengantar);
        if($pengantar->count() > 0) {
            $pengantar = $pengantar->first();
        } else {
            $pengantar = [
                'ucp_judul' => '',
                'ucp_isi' => ''
            ];
        }

        $penutup = mUcapanPenutup::where('id_ucapan_penutup', $order->id_ucapan_penutup);
        if($penutup->count() > 0) {
            $penutup = $penutup->first();
        } else {
            $penutup = [
                'uct_judul' => '',
                'uct_isi' => ''
            ];
        }

        $mantra = mMantraPenutup::where('id_mantra_penutup', $order->id_mantra_penutup);
        if($mantra->count() > 0) {
            $mantra = $mantra->first();
        } else {
            $mantra = [
                'mpp_judul' => '',
                'mpp_isi' => ''
            ];
        }

        $galeri = mOrderGaleriAcara::where('id_order', $order->id_order);
        if($galeri->count() > 0) {
            $galeri = $galeri->get();
        } else {
            $galeri = [];
        }

        $video = mOrderVideo::where('id_order', $order->id_order)->orderBy('id_order_video', 'ASC')->get();

        $buku_tamu = mOrderBukuTamu::where('id_order', $order->id_order)->orderBy('id_order_buku_tamu', 'DESC');
        if($buku_tamu->count() > 0) {
            $buku_tamu = $buku_tamu->get();
        } else {
            $buku_tamu = [];
        }

        $tamu_undangan = mOrderTamuUndangan::where(['id_order_tamu_undangan' => $id_order_tamu_undangan]);
        if($tamu_undangan->count() > 0) {
            $tamu_undangan = $tamu_undangan->first();
        } else {
            $tamu_undangan = [
                'otu_nama' => '',
                'otu_waktu' => ''
            ];
        }

        $mempelai_kategori = mOrderMempelaiKategori
            ::with([
                'mempelai'
            ])
            ->where([
                'id_order' => $order->id_order,
                'omk_status' => 'used'
            ])
            ->orderBy('id_order_mempelai_kategori', 'ASC')
            ->get();

        $data = [
            'order' => $order,
            'backsound' => $backsound,
            'cover' => $cover,
            'lokasi_waktu' => $lokasi_waktu,
            'mempelai' => $mempelai,
            'pengantar' => $pengantar,
            'penutup' => $penutup,
            'mantra' => $mantra,
            'galeri' => $galeri,
            'video' => $video,
            'buku_tamu' => $buku_tamu,
            'tamu_undangan' => $tamu_undangan,
            'mempelai_kategori' => $mempelai_kategori
        ];

        return view('undangan.design_2', $data);
    }

    function buku_tamu(Request $request, $ord_subdomain)
    {
        $request->validate([
            'nama' => 'required',
            'ucapan' => 'required',
            'kehadiran' => 'required'
        ]);


        $order = mOrder::where('ord_subdomain', $ord_subdomain)->first();
        $id_order = $order->id_order;
        $nama = $request->input('nama');
        $ucapan = $request->input('ucapan');
        $kehadiran = $request->input('kehadiran');

        $data_insert = [
            'id_order' => $id_order,
            'obt_nama' => $nama,
            'obt_ucapan' => $ucapan,
            'obt_hadir_status' => $kehadiran
        ];

        mOrderBukuTamu::create($data_insert);

    }
}
