<?php

namespace app\Http\Controllers\Undangan;

use app\Models\mBacksound;
use app\Models\mOrderBukuTamu;
use app\Models\mOrderGaleriCover;
use app\Models\mOrderMempelai;
use app\Models\mOrderVideo;
use app\Models\mProtokolKesehatan;
use app\Models\mUcapanPengantar;
use app\Models\mUcapanPenutup;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class BukuTamu extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['dashboard'];
        $this->breadcrumb = [];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $order = mOrder::where('id_order', $id_order)->first();

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dashboardOrder', ['username' => $order->ord_subdomain])
            ],
            [
                'label' => 'Buku Tamu',
                'route' => ''
            ]
        ]);

        $data = Main::data($breadcrumb, $this->menuActive);
        $data_list = mOrderBukuTamu::where('id_order', $id_order)->orderBy('id_order_buku_tamu', 'DESC')->get();

        $data = array_merge($data, [
            'data' => $data_list,
        ]);

        return view('dataUndangan/bukuTamu/bukuTamuList', $data);
    }

}
