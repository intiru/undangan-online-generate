<?php

namespace app\Http\Controllers\Undangan;

use app\Models\mMantraPenutup;
use app\Models\mOrder;
use app\Models\mOrderTamuUndangan;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class Dashboard extends Controller
{
    private $breadcrumb;
    private $menuActive;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['dashboard'];
        $this->breadcrumb = [];
    }

    function index()
    {

        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $order = mOrder::where('id_order', $id_order)->first();

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ]
        ]);

        $data = Main::data($breadcrumb, $this->menuActive);
        $data_list = mOrderTamuUndangan
            ::where('id_order', $id_order)
            ->orderBy('id_order_tamu_undangan', 'ASC')
            ->get();

        $data = array_merge($data, [
            'data' => $data_list,
            'order' => $order
        ]);

        return view('undangan/undanganDashboard', $data);
    }

    function login_process($subdomain)
    {
        $check = mOrder::where('ord_subdomain', $subdomain)->count();
        if ($check == 0) {
            return redirect()->route('frontHome', ['undangan' => 'login_gagal']);
        }

        $order = mOrder::where('ord_subdomain', $subdomain)->first();
        $level = 'order';

        Session::put([
            'level' => $level,
            'login' => TRUE,
            'order' => $order
        ]);

        return redirect()->route('dashboardOrder', ['username' => $order->ord_subdomain]);
    }

    function logout()
    {
        $order = Session::get('order');
        Session::flush();
        return redirect()->route('undanganLink', ['username' => $order['ord_subdomain']]);
    }

}
