<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mAction;
use app\Models\mBacksound;
use app\Models\mOrder;
use app\Models\mOrderVideo;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class VideoUndangan extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->menuActive = $cons['data_undangan'];
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');
        $data_list = mOrderVideo
            ::where('id_order', $id_order)
            ->orderBy('id_order_video', 'ASC')
            ->get();
        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Video Undangan',
                'route' => ''
            ]
        ]);

        $data = Main::data($breadcrumb, $this->menuActive);
        $data = array_merge($data, [
            'data' => $data_list
        ]);

        return view('dataUndangan.videoUndangan.videoUndanganList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'orv_tipe' => 'required',
        ]);

        $data_insert = $request->except('_token');
        $data_insert['id_order'] = Session::get('order')['id_order'];


        if ($request->hasFile('orv_filename')) {
            $file = $request->file('orv_filename');
            $filename = Main::filename($file);
            $file->move('upload/video_undangan', $filename);
            $data_insert['orv_filename'] = $filename;
        }

        mOrderVideo::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mOrderVideo::where('id_order_video', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataUndangan.videoUndangan.videoUndanganEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $orv_filename = mOrderVideo::where('id_order_video', $id)->value('orv_filename');
        if (File::exists('upload/video_undangan/' . $orv_filename)) {
            File::delete('upload/video_undangan/' . $orv_filename);
        }
        mOrderVideo::where('id_order_video', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'orv_tipe' => 'required',
        ]);
        $data = $request->except("_token");

        if ($request->hasFile('orv_filename')) {
            $orv_filename = mOrderVideo::where('id_order_video', $id)->value('orv_filename');
            if (File::exists('upload/video_undangan/' . $orv_filename)) {
                File::delete('upload/video_undangan/' . $orv_filename);
            }

            $file = $request->file('orv_filename');
            $filename = Main::filename($file);
            $file->move('upload/video_undangan', $filename);
            $data['orv_filename'] = $filename;
        }

        mOrderVideo::where(['id_order_video' => $id])->update($data);
    }
}
