<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mOrderMempelai;
use app\Models\mOrderMempelaiKategori;
use app\Models\mProtokolKesehatan;
use app\Models\mUcapanPengantar;
use app\Models\mUcapanPenutup;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class Mempelai extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Data Mempelai',
                'route' => ''
            ]
        ]);


        $data = Main::data($breadcrumb);
        $data_list = mOrderMempelai
            ::leftJoin('order_mempelai_kategori', 'order_mempelai_kategori.id_order_mempelai_kategori', '=', 'order_mempelai.id_order_mempelai_kategori')
            ->where('order_mempelai.id_order', $id_order)
            ->orderBy('order_mempelai.id_order_mempelai', 'ASC')
            ->get();
        $mempelai_kategori = mOrderMempelaiKategori
            ::where('id_order', $id_order)
            ->orderBy('id_order_mempelai_kategori', 'ASC')
            ->get();
        $tab_active = 'mempelai';

        $data = array_merge($data, [
            'data' => $data_list,
            'tab_active' => $tab_active,
            'mempelai_kategori' => $mempelai_kategori
        ]);

        return view('dataUndangan/mempelai/mempelaiList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'opl_foto' => 'max:900',
            'opl_nama_mempelai' => 'required',
        ]);

        $data = $request->except('_token');
        $data['id_order'] = Session::get('order')['id_order'];

        if($request->hasFile('opl_foto')) {
            $file = $request->file('opl_foto');
            $filename = Main::filename($file);
            $file->move('upload/mempelai', $filename);
            $data['opl_foto'] = $filename;
        }

        mOrderMempelai::create($data);

    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mOrderMempelai::where('id_order_mempelai', $id)->first();
        $mempelai_kategori = mOrderMempelaiKategori
            ::where('id_order', $edit->id_order)
            ->orderBy('id_order_mempelai_kategori', 'ASC')
            ->get();
        $data = [
            'edit' => $edit,
            'mempelai_kategori' => $mempelai_kategori
        ];

        return view('dataUndangan/mempelai/mempelaiEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        $opl_foto = mOrderMempelai::where('id_order_mempelai', $id)->value('opl_foto');
        if($opl_foto) {
            File::delete('upload/mempelai/' . $opl_foto);
        }

        mOrderMempelai::where('id_order_mempelai', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $request->validate([
            'opl_foto' => 'max:900',
            'opl_nama_mempelai' => 'required',
        ]);

        $data = $request->except('_token');
        $id_order_mempelai = Main::decrypt($id);

        if($request->hasFile('opl_foto')) {
            $opl_foto = mOrderMempelai::where('id_order_mempelai', $id_order_mempelai)->value('opl_foto');
            File::delete('upload/mempelai/' . $opl_foto);

            $file = $request->file('opl_foto');
            $filename = Main::filename($file);
            $file->move('upload/mempelai', $filename);
            $data['opl_foto'] = $filename;
        }

        mOrderMempelai::where('id_order_mempelai', $id_order_mempelai)->update($data);
    }

}
