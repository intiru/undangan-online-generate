<?php

namespace app\Http\Controllers\DataUndangan;

use app\Models\mOrderMempelaiKategori;
use app\Models\mUcapanPengantar;
use app\Models\mOrder;
use Illuminate\Http\Request;
use app\Http\Controllers\Controller;
use app\Helpers\Main;
use Illuminate\Support\Facades\Config;

use app\Models\mUser;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;

class MempelaiKategori extends Controller
{
    private $breadcrumb;

    function __construct()
    {
        $cons = Config::get('constants.topMenu');
        $this->breadcrumb = [
            [
                'label' => $cons['data_undangan'],
                'route' => route('dataUndanganList')
            ]
        ];
    }

    function index()
    {
        $id_order = Session::get('order')['id_order'];
        $ord_nama = mOrder::where('id_order', $id_order)->value('ord_nama');

        $breadcrumb = array_merge($this->breadcrumb, [
            [
                'label' => $ord_nama,
                'route' => route('dataUndanganMenu')
            ],
            [
                'label' => 'Data Mempelai',
                'route' => ''
            ]
        ]);
        $data = Main::data($breadcrumb);
        $data_list = mOrderMempelaiKategori
            ::where('id_order', $id_order)
            ->orderBy('id_order_mempelai_kategori', 'ASC')
            ->get();
        $tab_active = 'mempelai_kategori';

        $data = array_merge($data, [
            'data' => $data_list,
            'tab_active' => $tab_active
        ]);

        return view('dataUndangan.mempelaiKategori.mempelaiKategoriList', $data);
    }

    function insert(Request $request)
    {
        $request->validate([
            'omk_nama' => 'required',
            'omk_judul' => 'required',
        ]);

        $id_order = Session::get('order')['id_order'];
        $data_insert = $request->except('_token');
        $data_insert['id_order'] = $id_order;

        mOrderMempelaiKategori::create($data_insert);
    }

    function edit_modal($id)
    {
        $id = Main::decrypt($id);
        $edit = mOrderMempelaiKategori::where('id_order_mempelai_kategori', $id)->first();
        $data = [
            'edit' => $edit
        ];

        return view('dataUndangan/mempelaiKategori/mempelaiKategoriEditModal', $data);
    }

    function delete($id)
    {
        $id = Main::decrypt($id);
        mOrderMempelaiKategori::where('id_order_mempelai_kategori', $id)->delete();
    }

    function update(Request $request, $id)
    {
        $id = Main::decrypt($id);
        $request->validate([
            'omk_nama' => 'required',
            'omk_judul' => 'required',
        ]);
        $data = $request->except("_token");

        mOrderMempelaiKategori::where(['id_order_mempelai_kategori' => $id])->update($data);
    }
}
