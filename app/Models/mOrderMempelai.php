<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mOrderMempelai extends Model
{
    use SoftDeletes;

    protected $table = 'order_mempelai';
    protected $primaryKey = 'id_order_mempelai';
    protected $fillable = [
        'id_order',
        'id_order_mempelai_kategori',
        'opl_foto',
        'opl_nama_mempelai',
        'opl_nama_ayah',
        'opl_nama_ibu',
        'opl_urutan_anak',
        'opl_alamat',
        'opl_jenis_kelamin',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
