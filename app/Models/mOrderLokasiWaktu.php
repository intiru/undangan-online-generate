<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mOrderLokasiWaktu extends Model
{
    use SoftDeletes;

    protected $table = 'order_lokasi_waktu';
    protected $primaryKey = 'id_order_lokasi_waktu';
    protected $fillable = [
        'id_order',
        'olw_tanggal',
        'olw_jam_mulai',
        'olw_jam_selesai',
        'olw_jam_selesai_text',
        'olw_alamat',
        'olw_map_url',
        'olw_map_iframe',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
