<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mOrder extends Model
{
    use SoftDeletes;

    protected $table = 'order';
    protected $primaryKey = 'id_order';
    protected $fillable = [
        'ord_nama',
        'ord_alamat',
        'ord_keterangan',
        'ord_subdomain',
        'ord_status_aktif',
        'ord_judul',
        'id_ucapan_pengantar',
        'ord_ucapan_pengantar_status',
        'ord_ucapan_pengantar_text',
        'id_ucapan_pembuka',
        'ord_ucapan_pembuka_status',
        'ord_ucapan_pembuka_text',
        'id_ucapan_penutup',
        'ord_ucapan_penutup_status',
        'ord_ucapan_penutup_text',
        'id_mantra_penutup',
        'ord_mantra_penutup_status',
        'ord_mantra_penutup_text',
        'id_backsound',
        'id_message_chat',
        'ord_notifikasi_status',
        'ord_thumbnail',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
