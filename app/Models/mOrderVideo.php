<?php

namespace app\Models;

use app\Helpers\Main;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use function foo\func;

class mOrderVideo extends Model
{
    use SoftDeletes;

    protected $table = 'order_video';
    protected $primaryKey = 'id_order_video';
    protected $fillable = [
        'id_order',
        'orv_judul',
        'orv_isi',
        'orv_iframe_url',
        'orv_tipe',
        'orv_filename',
    ];

    public function getCreatedAtAttribute()
    {
        return date(Main::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }
}
