<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description"
          content="{{ \app\Helpers\Main::format_date_day($lokasi_waktu['olw_tanggal']) }}, {{ $lokasi_waktu['olw_alamat'] }}, Mulai {{ $tamu_undangan['otu_waktu'] ? $tamu_undangan['otu_waktu'] : Main::time_format_view($lokasi_waktu) }}">
    <title>Undangan Pernikahan | {{ $order['ord_nama'] }}</title>

    <meta property="og:title" content="{{ $order['ord_judul'] }} {!! $order['ord_nama'] !!} ">
    <meta property="og:type" content="Website">
    <meta property="og:image" content="{{ asset('upload/thumbnail_undangan/'.$order['ord_thumbnail']) }}">
    <meta property="og:url" content="https://intiru.com">
    <meta property="og:site_name" content="Undangan Online">
    <meta property="og:description"
          content="{{ \app\Helpers\Main::format_date_day($lokasi_waktu['olw_tanggal']) }}, {{ $lokasi_waktu['olw_alamat'] }}, Mulai {{ $tamu_undangan['otu_waktu'] ? $tamu_undangan['otu_waktu'] : Main::time_format_view($lokasi_waktu) }}">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;1,100;1,200;1,300;1,400&family=Spectral:ital,wght@0,300;0,400;0,500;0,600;1,300;1,400;1,500;1,600&display=swap"
          rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('design_undangan/2/css/vendors/bootstrap.min.css') }}">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('design_undangan/2/css/vendors/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('design_undangan/2/css/vendors/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('design_undangan/2/css/style.css') }}">
    <link href="{{ asset('css/loading.css') }}" rel="stylesheet" type="text/css"/>

    <!-- Favicons -->
    <link href="{{ asset('upload/thumbnail_undangan/'.$order['ord_thumbnail']) }}" rel="icon">
    <link href="{{ asset('upload/thumbnail_undangan/'.$order['ord_thumbnail']) }}" rel="apple-touch-icon">


</head>
<body>
<img src="{{ asset('upload/thumbnail_undangan/'.$order['ord_thumbnail']) }}" class="d-none">

<div id="buttonmusic">
    <ion-icon name="volume-high-outline"></ion-icon>
</div>
<audio id="player">
    <source src="{{ asset('upload/backsound/'.$backsound['bks_filename']) }}" type="audio/mpeg">
</audio>

<header>
    <div class="js-hero-slider">
        @foreach($cover as $key => $row)
            <div class="hero js-hero-slide d-flex align-items-center justify-content-center"
                 style="background-image: linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url('{{ asset('upload/galeri_cover/'.$row->ogr_gambar) }}')">
            </div>
        @endforeach
    </div>
    <div class="hero-body">
        <h1 class="hero_title">{{ $order['ord_nama'] }}</h1>
        <p class="hero_date">{{ date('d / m / Y', strtotime($lokasi_waktu['olw_tanggal'])) }}</p>
    </div>
    <div class="hero-tamu">
        <p class="paragraph-modal">Kepada</p>
        <p class="paragraph-modal">Bapak/Ibu/Saudara/i</p>
        <h2 class="modal-penerima">{{ $tamu_undangan['otu_nama'] }}</h2>
        <a href="{{ url('#intro') }}" class="btn btn-md btn-default btn-modal smoothScroll" id="btn-open">Buka
            Undangan</a>
    </div>
</header>


<section class="intro" id="intro">

    <div class="border-background-first"></div>
    <div class="border-background">
        <img src="{{ asset('design_undangan/2/img/ukiran-background.png') }}"></img>
    </div>
    <div class="container">

        <div class="row">
            @foreach($mempelai_kategori as $row)
                <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
                    <br/>
                    <br/>
                    <br/>
                    <h2 class="heading-text" data-aos="zoom-in-up" data-aos-delay="50">{{ $row->omk_judul }}</h2>
                    @if($row->omk_isi)
                        <p class="cmb-3 font-italic text-center" data-aos="zoom-in-up"
                           data-aos-delay="300">{!! $row->omk_isi !!}</p>
                    @endif
                </div>

                @foreach($row->mempelai as $row_mempelai)
                    <div class="col-sm-6 mempelai" data-aos="fade-right">
                        @if($row_mempelai->opl_foto)
                            <div class="mempelai-foto">
                                <img class="frame" src="{{ asset('upload/mempelai/'.$row_mempelai->opl_foto) }}">
                            </div>
                        @endif

                        <br>
                        <div class="mempelai-nama">{{ $row_mempelai->opl_nama_mempelai }}</div>
                        <div class="mempelai-detail">
                            {{ ucwords($row_mempelai->opl_jenis_kelamin) }} {{ $row_mempelai->opl_urutan_anak }}

                            @if($row_mempelai->opl_nama_ayah || $row_mempelai->opl_nama_ibu)
                                dari
                                Pasangan
                            @endif
                        </div>
                        @if($row_mempelai->opl_nama_ayah || $row_mempelai->opl_nama_ibu)
                            <div class="mempelai-ortu">{{ $row_mempelai->opl_nama_ayah }}
                                <br>&amp;<br> {{ $row_mempelai->opl_nama_ibu }}
                            </div>
                        @endif
                        @if($row_mempelai->opl_alamat)
                            <div class="mempelai-alamat">{{ $row_mempelai->opl_alamat }}</div>
                        @endif
                    </div>
                @endforeach
            @endforeach

        </div>
    </div>
    {{--    <div class="intro-shape">--}}
    {{--        <svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">--}}
    {{--            <path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z"--}}
    {{--                  class="shape-fill"></path>--}}
    {{--        </svg>--}}
    {{--    </div>--}}
</section>
<div class="border-background-first"></div>
<div class="border-background">
    <img src="{{ asset('design_undangan/2/img/ukiran-background.png') }}"></img>
</div>

<section class="location js-section" id="location">
    <h2 class="heading-text" data-aos="fade-up">{{ $pengantar['ucp_judul'] }}</h2>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="cmb-2 font-italic text-center" data-aos="fade-up" data-aos-delay="300">
                    {!!  strip_tags($pengantar['ucp_isi'])  !!}
                </p>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-12 location_box" data-aos="zoom-in-up">
                <div class="location-acara resepsi"
                     style="background-image: linear-gradient(rgba(255,255,255,0.8), rgba(255,255,255,0.8)), url('{{ asset('upload/galeri_cover/'.$cover[0]->ogr_gambar) }}')">
                    <div class="card-title">
                        <img src="{{ asset('design_undangan/2/img/event-icon.png') }}" class="acara-icon" alt=""/>
                    </div>
                    <h2 class="title">Resepsi</h2>
                    <h2 class="acara-head">{{ Main::day_id(date('l', strtotime($lokasi_waktu['olw_tanggal']))) }}</h2>
                    <p class="acara-detail">{{ Main::date_id($lokasi_waktu['olw_tanggal']) }}</p>
                    <h2 class="acara-head">Pukul</h2>
                    <p class="acara-detail">
                        @if($tamu_undangan['otu_waktu'])
                            {{ $tamu_undangan['otu_waktu'] }}
                        @else
                            {{ Main::time_format_view($lokasi_waktu) }}
                        @endif
                    </p>
                    <p class="acara-detail">Bertempat di</p>
                    <p class="acara-alamat">
                        {{ $lokasi_waktu['olw_alamat'] }}
                    </p>
                    <div class="aos-init aos-animate">
                        <a href="{{ $lokasi_waktu['olw_map_url'] }}" target="_blank">
                            <br>
                            <button class="btn btn-md btn-default btn-map">Map Lokasi Acara</button>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-md-12 cmb-2 font-italic" data-aos="fade-up">
                {!!  $penutup['uct_isi']  !!}
            </div>
        </div>
    </div>
</section>

<section class="countdown cbg-grey" id="countdown">
    <div class="container">

        <div class="row countdown-box" data-aos="zoom-in-up">
            <div class="col-md-12">
                <h2 class="heading-text" data-aos="zoom-in-up" data-aos-delay="50">Hari Bahagia Kami</h2>
            </div>
            <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2" data-aos="zoom-in-up">
                <div class="countdown-list js-countdown"
                     data-date="{{ date('Y/m/d H:i', strtotime($lokasi_waktu['olw_tanggal'].' '.$lokasi_waktu['olw_jam_mulai'])) }} ">
                    <div>
                        <div class="js-countdown-days">0</div>
                        <p>HARI</p>
                    </div>
                    <div>
                        <div class="js-countdown-hours">0</div>
                        <p>JAM</p>
                    </div>
                    <div>
                        <div class="js-countdown-minutes">0</div>
                        <p>MENIT</p>
                    </div>
                    <div>
                        <div class="js-countdown-seconds">0</div>
                        <p>DETIK</p>
                    </div>
                </div>
                <div class="col-md-12">
                    <p class="cmb-2 cmt-2 font-italic text-center" data-aos="zoom-in-up" data-aos-delay="50">Sampai
                        ketemu di hari bahagia kami.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="quote">
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-md-1 text-center">
                <h3 class="quote__text font-italic" data-aos="fade-up">
                    {!! $mantra['mpp_isi'] !!}
                </h3>
            </div>
        </div>
    </div>
</section>
<div class="border-background-first"></div>
<div class="border-background">
    <img src="{{ asset('design_undangan/2/img/ukiran-background.png') }}">
</div>

<section class="stories text-center js-section" id="stories">
    <div class="container">
        <div class="row cmb-3">
            <div class="col-sm-12">
                <h2 class="heading-text" data-aos="fade-up">Momen Bahagia</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="grid" data-aos="fade-up" data-aos-delay="300">
                    <div class="grid-sizer"></div>
                    <div class="gutter-sizer"></div>
                    @foreach($galeri as $row)
                        <div class="grid-item">
                            <a class="story-popup" href="{{ asset('upload/galeri_undangan/'.$row->oga_gambar) }}"><img
                                        src="{{ asset('upload/galeri_undangan/'.$row->oga_gambar) }}"
                                        class="stories__img"
                                        alt="story1"/></a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@foreach($video as $row)
    <section class="wrapper-video video text-center js-section" id="video">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <h2 class="heading-text cmb-3" data-aos="fade-up">{{ $row->orv_judul }}</h2>
                    <p>{{ $row->orv_isi }}</p>
                    <img class="hiasan top" src="{{ asset('design_undangan/2/img/ukiran-background.png') }}">
                    <div class="wrapper-video-2">
                        <div class="wrapper-video-1 col-md-12" data-aos="fade-up">
                            @if($row->orv_tipe == 'youtube')
                            {!! $row->orv_iframe_url !!}
                            @elseif($row->orv_tipe == 'file')
                                <video width="100%" height="auto" controls>
                                    <source src="{{ asset('upload/video_undangan/'.$row->orv_filename) }}"
                                            type="video/mp4">
                                    <source src="{{ asset('upload/video_undangan/'.$row->orv_filename) }}"
                                            type="video/ogg">
                                    Your browser does not support the video tag.
                                </video>
                            @endif
                        </div>
                    </div>
                    <img class="hiasan" src="{{ asset('design_undangan/2/img/ukiran-background.png') }}">
                </div>
            </div>
        </div>
    </section>
@endforeach

<section class="ucapan text-center js-section">
    <div class="container">
        <div class="row cmb-3">
            <div class="col-md-12 box-ucapan top">
                <center>
                    <img src="{{ asset('design_undangan/2/img/ukiran-background.png') }}">
                </center>
            </div>
            <div class="col-md-8 offset-md-2 ucapan-box" data-aos="zoom-in-up">
                <div class="ucapan-box-2">
                    <h2 class="heading-text text-center cmb-3">Mohon Doa Restu</h2>
                    <form class="rsvp-form form-send"
                          action="{{  route('bukuTamuInsert', ['ord_subdomain' => $order['ord_subdomain']]) }}"
                          method="post"
                          data-alert-show="true"
                          data-alert-field-message="true"
                          data-alert-show-success-status="true"
                          data-alert-show-success-title="Berhasil"
                          data-alert-show-success-message="Ucapan anda berhasil terkirim, Terima Kasih">
                        {{ csrf_field() }}
                        <div class="d-md-flex justify-content-between">
                            <div class="col-sm-12">
                                <div class="rsvp-form-field">
                                    <label for="nama">Masukan Namamu</label>
                                    <input type="text" name="nama" id="nama">
                                </div>
                                <div class="rsvp-form-field textarea">
                                    <label for="ucapan">Berikan Ucapanmu</label>
                                    <textarea rows="3" name="ucapan" id="ucapan"></textarea>
                                </div>
                                <div class="rsvp-form-field">
                                    <label>Kamu akan hadir?</label>
                                    <div class="radio-wrapper">
                                        <input type="radio" checked="" id="radio-1" name="kehadiran" value="Hadir">
                                        <label for="radio-1">Hadir</label>
                                        <input type="radio" id="radio-2" name="kehadiran" value="Tidak Hadir">
                                        <label for="radio-2">Tidak Hadir</label>
                                        <input type="radio" id="radio-3" name="kehadiran" value="Masih Ragu">
                                        <label for="radio-3">Masih Ragu</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <button type="submit" class="rsvp-form-submit">Kirim Ucapan</button>
                    </form>
                </div>
            </div>

            <div class="col-md-12 box-ucapan">
                <center>
                    <img src="{{ asset('design_undangan/2/img/ukiran-background.png') }}">
                </center>
            </div>
        </div>
        <div class="row" id='kolom-ucapan'>
            <div class='col-md-12 cmt-5'>
                <h2 class='heading-text' data-aos='fade-up'>Ucapan Doa</h2>
            </div>
            <div class='col-sm-12' data-aos='fade-up'>
                <div class='scroll col-md-10 offset-md-1' data-aos='fade-up'>
                    <div class='box_ucapan'>

                        @foreach($buku_tamu as $row)
                            <div class='row_ucapan'>
                                <div class='card'>
                                    <div class='card-body'>
                                        <blockquote class=''><i class='fa fa-quote-right fa-2x pull-right mt-3 mb-3'
                                            <p>{{ $row->obt_ucapan }}</p>
                                            <footer class='blockquote-footer small p-1'><span
                                                        class='ucapan_nama'>{{ $row->obt_nama }}<br><cite
                                                            class='ucapan-hadir'>{{ Main::obt_hadir_status($row->obt_hadir_status) }}</cite></span>
                                            </footer>
                                        </blockquote>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="footer-hiasan">
    <img src="{{ asset('design_undangan/2/img/ukiran-background.png') }}">
</div>
<div class="footer-top"></div>
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="container col-md-8 col-sm-10 protokol">
                <div class="col-md-12 col-sm-12 text-center">
                    <h1 class="heading">Protokol Kesehatan</h1>
                    <p class="cmb-3">Harap tetap mematuhi protokol kesehatan dengan:</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 protokol-item">
                    <img src="{{ asset('design_undangan/2/img/health/health-1.png') }}"/>
                    <p>Jaga Area Wajah</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 protokol-item">
                    <img src="{{ asset('design_undangan/2/img/health/health-2.png') }}"/>
                    <p>Hindari Kerumunan</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 protokol-item">
                    <img src="{{ asset('design_undangan/2/img/health/health-3.png') }}"/>
                    <p>Cuci Tangan</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 protokol-item">
                    <img src="{{ asset('design_undangan/2/img/health/health-4.png') }}"/>
                    <p>Jaga Jarak</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 protokol-item">
                    <img src="{{ asset('design_undangan/2/img/health/health-5.png') }}"/>
                    <p>Jaga HP tetap Bersih</p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 protokol-item">
                    <img src="{{ asset('design_undangan/2/img/health/health-6.png') }}"/>
                    <p>Gunakan Masker</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <h2 class="heading-text">Terima Kasih</h2>
                    <ul class="footer__social">
                        <li><a href="https://www.instagram.com/dimasmahendraputra/" target="_blank">
                                <ion-icon name="logo-instagram" class="icon icon--sm icon--white"></ion-icon>
                            </a></li>
                        <li><a href="https://www.facebook.com/azimuthstudiophotovideo" target="_blank">
                                <ion-icon name="logo-facebook" class="icon icon--sm icon--white"></ion-icon>
                            </a></li>
                        <li><a href="https://wa.me/+628970937114" target="_blank">
                                <ion-icon name="logo-whatsapp" class="icon icon--sm icon--white"></ion-icon>
                            </a></li>
                    </ul>
                    <p class="footer__copy">Undangan by Azimuth</p>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class='container-loading hidden'>
    <div class='loader'>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--dot'></div>
        <div class='loader--text'></div>
        <div class='loader--desc'></div>
    </div>
</div>

<script src="{{ asset('design_undangan/2/js/vendors/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('design_undangan/2/js/smoothscroll.js') }}"></script>
<script src="{{ asset('design_undangan/2/js/vendors/bootstrap.min.js') }}"></script>
<script src="{{ asset('design_undangan/2/js/vendors/jquery.countdown.min.js') }}"></script>
<script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="{{ asset('design_undangan/2/js/vendors/slick.min.js') }}"></script>
<script src="{{ asset('design_undangan/2/js/vendors/jquery.magnific-popup.min.js') }}"></script>
<script src="https://unpkg.com/ionicons@5.2.3/dist/ionicons.js"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script src="{{ asset('design_undangan/2/js/script.js') }}"></script>


</body>
</html>
