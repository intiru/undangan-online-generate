@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/crud/forms/widgets/select2.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-form m-form--label-align-right">
                    <div class="m-portlet__body row">
                        <div class="col-sm-12 col-md-8">
                            <div class="m-form__section m-form__section--first">
                                <div class="m-form__heading">
                                    <h2>
                                        {{ $order->ord_nama }}
                                    </h2>
                                    <h5><i class="la la-map-pin"></i> {{ $order->ord_alamat }}</h5>
                                    <p><i class="la la-info-circle"></i> {{ $order->ord_keterangan }}</p>
                                </div>
                                <div class="form-group m-form__group" style="margin-top: -16px">
                                    <label for="example_input_full_name">
                                        <a href="{{ route('undanganLink', ['username' => $order->ord_subdomain, 'id_order_tamu_undangan' => Main::encrypt(0)]) }}"
                                           target="_blank">
                                            <i class="la la-link"></i> {{ route('undanganLink', ['username' => $order->ord_subdomain]) }}
                                        </a>
                                    </label>
                                </div>
                                <button class="btn btn-info btn-order-copy-link" data-link="{{ route('undanganLink', ['username' => $order->ord_subdomain, 'id_order_tamu_undangan' => Main::encrypt(0)]) }}"><i class="la la-copy"></i> Salin Link Undangan</button>
                                <a href="{{ route('undanganLink', ['username' => $order->ord_subdomain, 'id_order_tamu_undangan' => Main::encrypt(0)]) }}" target="_blank" class="btn btn-default"><i class="la la-eye"></i> Lihat Undangan</a>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-4">
                            <table class="table table-bordered table-striped">
                                <tbody>
                                <tr>
                                    <td><i class="la la-eye"></i> Status Publish Undangan</td>
                                    <td>
                                        <span class="m-switch m-switch--outline m-switch--icon m-switch--success">
                                            <label>
                                                <input
                                                        type="checkbox"
                                                        class="edit-order-status"
                                                        data-route-order-status-aktif="{{ route('dataUndanganOrderAktif', ['id'=>Main::encrypt($order->id_order)]) }}"
                                                        {{ $order->ord_status_aktif == 'on' ? 'checked="checked"':'' }}  name="ord_status_aktif"
                                                        value="on">
                                                <span></span>
                                            </label>
                                        </span>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row app-menu-list">

                <a href="{{ route('orderBukuTamuList', ['username' => Session('order')['ord_subdomain']]) }}" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-book"></i>
                                <h3 align="center">Daftar Buku Tamu</h3>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('orderTamuUndanganList', ['username' => Session('order')['ord_subdomain']]) }}" class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-form m-form--label-align-right">
                            <div class="m-portlet__body">
                                <i class="la la-users"></i>
                                <h3 align="center">Daftar Tamu Undangan</h3>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

        </div>

    </div>
@endsection
