@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/summernote.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')
    @include('dataUndangan.tamuUndangan.tamuUndanganCreate')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
                <div>
                    <a href="#"
                       class="akses-create btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air akses-create"
                       data-toggle="modal" data-target="#modal-create">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Tambah Data</span>
                            </span>
                    </a>
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="m-portlet m-portlet--mobile akses-list">
                <div class="m-portlet__body">
                    <div class="table-responsive">
                        <table class="akses-list datatable-new-2 datatable table table-striped table-bordered table-hover table-checkable datatable-general">
                            <thead>
                            <tr>
                                <th width="20">No</th>
                                <th>Nama Undangan</th>
                                <th>Link Undangan</th>
{{--                                <th>No WhatsApp</th>--}}
{{--                                <th>Waktu Undangan</th>--}}
{{--                                <th>Alamat Tamu</th>--}}
{{--                                <th width="150">Created at</th>--}}
{{--                                <th width="150">Updated at</th>--}}
                                <th width="100">Menu</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($data as $key => $row)
                                <tr>
                                    <td align="center">{{ $key + 1 }}.</td>
                                    <td>{{ $row->otu_nama }}</td>
                                    <td>
                                        <textarea class="form-control">{{ route('undanganLink', ['username' => $order->ord_subdomain, 'id_order_tamu_undangan' => Main::encrypt_order($row->id_order_tamu_undangan)]) }}</textarea>
                                    </td>
{{--                                    <td>{{ $row->otu_no_whatsapp }}</td>--}}
{{--                                    <td>{{ $row->otu_waktu }}</td>--}}
{{--                                    <td>{{ $row->otu_alamat }} </td>--}}
{{--                                    <td>{{ $row->created_at }}</td>--}}
{{--                                    <td>{{ $row->updated_at }}</td>--}}
                                    <td>
                                        <div class="dropdown">
                                            <button class="btn btn-sm btn-accent dropdown-toggle m-btn--pill"
                                                    type="button"
                                                    id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                                    aria-expanded="false">
                                                Menu
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right"
                                                 aria-labelledby="dropdownMenuButton">
                                                <a href="{{ route('undanganLink', ['username' => $order->ord_subdomain, 'id_order_tamu_undangan' => Main::encrypt_order($row->id_order_tamu_undangan)]) }}"
                                                   class="dropdown-item" target="_blank">
                                                    <i class="la la-eye"></i> Lihat Undnagan
                                                </a>
                                                <a href="#" class="dropdown-item btn-order-copy-link"
                                                   data-link="{{ route('undanganLink', ['username' => $order->ord_subdomain, 'id_order_tamu_undangan' => Main::encrypt_order($row->id_order_tamu_undangan)]) }}">
                                                    <i class="la la-copy"></i> Copy Link Share
                                                </a>
                                                <div class="dropdown-divider"></div>
                                                <a class="akses-edit dropdown-item btn-modal-general"
                                                   data-route="{{ route('orderTamuUndanganEditModal', ['username' => $order->ord_subdomain, 'id'=> Main::encrypt($row->id_order_tamu_undangan)]) }}"
                                                   href="#">
                                                    <i class="la la-pencil"></i>
                                                    Edit
                                                </a>
                                                <a class="akses-delete dropdown-item btn-hapus"
                                                   href="#"
                                                   data-route="{{ route('orderTamuUndanganDelete', ['username' => $order->ord_subdomain, 'id' => Main::encrypt($row->id_order_tamu_undangan)]) }}">
                                                    <i class="la la-remove"></i>
                                                    Hapus
                                                </a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
