@extends('../general/index')


@section('js')
    <script src="{{ asset('assets/app/js/dashboard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>

    <script src="{{ asset('plugin/chart/chart.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('plugin/chart/utils.js') }}" type="text/javascript"></script>

    <script>
        var MONTHS = [
            @foreach($cart_order['label'] as $row)
                    {!! "'".date('d M', strtotime($row))."', " !!}
                    @endforeach
        ];
        var config_order = {
            type: 'line',
            data: {
                labels: [
                    @foreach($cart_order['label'] as $row)
                            {!! "'".date('d M', strtotime($row))."', " !!}
                            @endforeach
                ],
                datasets: [{
                    label: 'Order',
                    backgroundColor: window.chartColors.red,
                    borderColor: window.chartColors.red,
                    data: [
                        @foreach($cart_order['label'] as $row)
                                {{ $cart_order['data']['order'][$row].", " }}
                                @endforeach
                    ],
                    fill: false,
                }]
            },
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Chart.js Line Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value'
                        }
                    }]
                }
            }
        };

        window.onload = function () {
            var ctx_order = document.getElementById('chart-order').getContext('2d');
            window.myLine = new Chart(ctx_order, config_order);
        };

        function numberWithCommas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

    </script>

@endsection

@section('css')
    <link href="//www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css"/>
    <style>
        canvas {
            -moz-user-select: none;
            -webkit-user-select: none;
            -ms-user-select: none;
        }
    </style>
@endsection

@section('body')
    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-content">

            <div class="m-portlet m-portlet--tab">
                <form method="get" class="m-form m-form--fit m-form--label-align-right form-dashboard-filter">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group row text-center">
                            <label class="col-form-label col-lg-2 offset-lg-2 col-sm-12">Rentang Tanggal Data</label>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value="{{ $date_from }}"
                                       placeholder="Select time"
                                       name="date_from"/>
                            </div>
                            <div class="col-lg-2 col-md-9 col-sm-12">
                                <input type='text'
                                       class="form-control m_datepicker_1_modal"
                                       readonly
                                       value="{{ $date_to }}"
                                       placeholder="Select time"
                                       name="date_to"/>
                            </div>
                            <div class="col-lg-4 col-sm-12">
                                <button type="submit" class="btn btn-accent m-btn--pill">
                                    <i class="la la-search"></i> Filter Data
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>

            <div class="row">
                <a href="{{ route('dataUndanganList') }}" class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height m-portlet--rounded-force"
                         style=" margin: 0px">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light" style=" margin: 0px">
                                        Total Order
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('images/background-card.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span>{{ $total_order }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('dataUndanganList') }}" class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height m-portlet--rounded-force"
                         style=" margin: 0px">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light" style=" margin: 0px">
                                        Total Tamu Undangan
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('images/background-card.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span>{{ $total_tamu_undangan }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('dataUndanganList') }}" class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height m-portlet--rounded-force"
                         style=" margin: 0px">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light" style=" margin: 0px">
                                        Total Buku Tamu
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('images/background-card.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span>{{ $total_buku_tamu }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                <a href="{{ route('dataUndanganList') }}" class="col-xl-3">
                    <div class="m-portlet m-portlet--head-overlay m-portlet--full-height m-portlet--rounded-force"
                         style=" margin: 0px">
                        <div class="m-portlet__head m-portlet__head--fit-">
                            <div class="m-portlet__head-caption">
                                <div class="m-portlet__head-title">
                                    <h3 class="m-portlet__head-text m--font-light" style=" margin: 0px">
                                        Total Galeri Foto
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="m-portlet__body">
                            <div class="m-widget27 m-portlet-fit--sides">
                                <div class="m-widget27__pic">
                                    <img src="{{ asset('images/background-card.jpg') }}" alt="">
                                    <h3 class="m-widget27__title m--font-light">
                                        <span>{{ $total_galeri_foto }}</span>
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>

            <ul class="nav nav-tabs" role="tablist" style="margin: 0">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#" data-target="#m_tabs_1_2">
                        <h4><i class="la la-line-chart"></i> Grafik Order </h4>
                    </a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="m_tabs_1_2" role="tabpanel">
                    <div class="m-portlet  m-portlet--tab">
                        <div class="m-portlet__body">
                            <canvas id="chart-order" style="height: 200px !important;"></canvas>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
