<form action="{{ route('ucapanPengantarUpdate', ['id'=>Main::encrypt($edit->id_ucapan_pengantar)]) }}"
      enctype="multipart/form-data"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">

                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Ucapan</label>
                            <input type="text" class="form-control m-input" name="ucp_nama" value="{{ $edit->ucp_nama }}"
                                   autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Judul Ucapan</label>
                            <input type="text" class="form-control m-input" name="ucp_judul" value="{{ $edit->ucp_judul }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Isi Ucapan</label>
                            <textarea class="form-control m-input summernote" name="ucp_isi">{{ $edit->ucp_isi }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>