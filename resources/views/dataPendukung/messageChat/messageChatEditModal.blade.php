<form action="{{ route('messageChatUpdate', ['id'=>Main::encrypt($edit->id_message_chat)]) }}"
      enctype="multipart/form-data"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">

                        <strong>Gunakan daftar variable dibawah untuk mengganti data sesuai dengan keterangan variable :</strong>
                        <br />
                        <br />
                        <div class="m-alert m-alert--icon m-alert--outline alert alert-success alert-dismissible fade show"
                             role="alert" style="width: 100%">
                            <div class="m-alert__text">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Variable</th>
                                        <th>Keterangan</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($variable_list as $variable => $keterangan)
                                        <tr>
                                            <td>{{ $no++ }}</td>
                                            <td>{{ $variable }}</td>
                                            <td>{{ $keterangan }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Judul Ucapan</label>
                            <input type="text" class="form-control m-input" name="msc_judul" value="{{ $edit->msc_judul }}"
                                   autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Isi Ucapan</label>
                            <textarea class="form-control m-input" name="msc_isi" rows="15">{{ $edit->msc_isi }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>