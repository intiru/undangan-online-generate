@extends('../general/index')

@section('css')

@endsection

@section('js')

@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="row">
                <div class="col-sm-12 col-md-4">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            <form action="{{ route('undanganVideoUpdate') }}"
                                  enctype="multipart/form-data"
                                  method="post"
                                  class="form-send"
                                  data-alert-show="true"
                                  data-alert-field-message="true">
                                {{ csrf_field() }}
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Tipe Video :
                                        </label>
                                        <select name="orv_type" class="form-control">
                                            <option value="iframe">Iframe Youtube/Video</option>
                                            <option value="file">File Video</option>
                                        </select>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Iframe Video Youtube:
                                        </label>
                                        <textarea class="form-control" name="orv_iframe_url">{{ $row['orv_iframe_url'] }}</textarea>
                                    </div>
                                </div>

                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group text-center">
                                        <button type="submit" class="btn m-btn--pill btn-success">
                                            <i class="la la-check"></i> Perbarui
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-8">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            {!! $row['orv_iframe_url'] !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
