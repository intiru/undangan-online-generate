<form action="{{ route('undanganVideoInsert') }}"
      enctype="multipart/form-data"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}
    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Judul Video</label>
                            <input type="text" class="form-control m-input" name="orv_judul" autofocus>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Keterangan Video</label>
                            <textarea class="form-control m-input" name="orv_isi"></textarea>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Tipe Video</label>
                            <select class="form-control" name="orv_tipe">
                                <option value="youtube">Youtube Iframe</option>
                                <option value="file">File</option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">File Video</label>
                            <br />
                            <input type="file" class="m-input" name="orv_filename" accept="video/*, audio/*">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kode Iframe Youtube Video</label>
                            <br />
                            <textarea class="form-control m-input" name="orv_iframe_url"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-simpan btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>