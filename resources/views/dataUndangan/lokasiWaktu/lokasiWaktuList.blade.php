@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-datetimepicker.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/demo/default/custom/components/forms/widgets/bootstrap-timepicker.js') }}"
            type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="row">
                <div class="col-sm-12 col-md-6 offset-md-3">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            <form action="{{ route('undanganLokasiWaktuUpdate') }}"
                                  enctype="multipart/form-data"
                                  method="post"
                                  class="form-send"
                                  data-alert-show="true"
                                  data-alert-field-message="true">
                                {{ csrf_field() }}
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name" class="required">
                                            Tanggal Undangan
                                        </label>
                                        <input type="text" readonly class="form-control m-input m_datepicker_1_modal" name="olw_tanggal" value="{{ $row['olw_tanggal'] ? Main::format_date($row['olw_tanggal']) : date('d-m-Y') }}">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name" class="required">
                                            Jam Mulai Undangan
                                        </label>
                                        <input type="text" readonly class="form-control m-input m_timepicker_2" name="olw_jam_mulai" value="{{ $row['olw_jam_mulai'] ? Main::format_time_db($row['olw_jam_mulai']): '' }}">
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name" class="required">
                                            Jam Selesai Undangan
                                        </label>
                                        <input type="text" readonly class="form-control m-input m_timepicker_2" name="olw_jam_selesai" value="{{ $row['olw_jam_selesai'] ? Main::format_time_db($row['olw_jam_selesai']): '' }}">
                                        <div class="m-checkbox-list" style="margin-top: 6px">
                                            <label class="m-checkbox">
                                                <input type="checkbox" name="olw_jam_selesai_text" {{ $row['olw_jam_selesai_text'] == 'yes' ? 'checked':'' }} value="yes">
                                                Isi dengan "<strong>Sampai Selesai</strong>"
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name" class="required">
                                            Alamat Acara
                                        </label>
                                        <textarea class="form-control m-input" name="olw_alamat">{{ $row['olw_alamat'] }}</textarea>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name" class="required">
                                            URL Google Maps Acara
                                        </label>
                                        <textarea class="form-control m-input" name="olw_map_url">{{ $row['olw_map_url'] }}</textarea>
                                    </div>
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name" class="required">
                                            iframe Google Maps Acara
                                        </label>
                                        <textarea class="form-control m-input" name="olw_map_iframe">{{ $row['olw_map_iframe'] }}</textarea>
                                    </div>
                                </div>

                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group text-center">
                                        <button type="submit" class="btn m-btn--pill btn-success btn-simpan">
                                            <i class="la la-check"></i> Perbarui
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
