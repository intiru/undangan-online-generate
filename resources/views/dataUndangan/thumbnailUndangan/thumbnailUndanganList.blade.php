@extends('../general/index')

@section('css')

@endsection

@section('js')

@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            <form action="{{ route('undanganThumbnailUpdate') }}"
                                  enctype="multipart/form-data"
                                  method="post"
                                  class="form-send"
                                  data-alert-show="true"
                                  data-alert-field-message="true">
                                {{ csrf_field() }}
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Memilih File Thumbnail:
                                        </label>
                                        <input type="file" class="form-control m-input input-file-browse" name="ord_thumbnail">
                                        <span class="m-form__help">Maksimal Upload File Gambar 900KB.</span>
                                    </div>
                                </div>

                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group text-center">
                                        <button type="submit" class="btn m-btn--pill btn-success">
                                            <i class="la la-check"></i> Perbarui Thumbnail
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            <img src="{{ asset('upload/thumbnail_undangan/'.$order->ord_thumbnail) }}" class="img-fluid img-preview">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
