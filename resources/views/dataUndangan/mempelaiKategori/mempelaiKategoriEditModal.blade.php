<form action="{{ route('mempelaiKategoriUpdate', ['id'=>Main::encrypt($edit->id_order_mempelai_kategori)]) }}"
      enctype="multipart/form-data"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">
    {{ csrf_field() }}
    <div class="modal" id="modal-general" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Status</label>
                            <select class="form-control" name="omk_status">
                                <option value="used" {{ $edit->omk_status == 'used' ? 'selected':'' }}>Digunakan</option>
                                <option value="not_used" {{ $edit->omk_status == 'not_used' ? 'selected':'' }}>Tidak Digunakan</option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Kategori Mempelai</label>
                            <input type="text" class="form-control m-input" name="omk_nama" value="{{ $edit->omk_nama }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Judul Kategori Mempelai</label>
                            <input type="text" class="form-control m-input" name="omk_judul" value="{{ $edit->omk_judul }}">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Isi Kategori Mempelai</label>
                            <textarea class="form-control m-input summernote" name="omk_isi">{{ $edit->omk_isi }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-update btn-success">Perbarui</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>