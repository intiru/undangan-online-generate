@extends('../general/index')

@section('css')
    <link href="{{ asset('assets/vendors/custom/datatables/datatables.bundle.css') }}" rel="stylesheet"
          type="text/css"/>
@endsection

@section('js')
    <script src="{{ asset('assets/vendors/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">

            <div class="row">
                <div class="col-sm-12 col-md-3">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            <form action="{{ route('undanganUcapanPengantarUpdate') }}"
                                  enctype="multipart/form-data"
                                  method="post"
                                  class="form-send"
                                  data-alert-show="true"
                                  data-alert-field-message="true">
                                {{ csrf_field() }}
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group">
                                        <label for="example_input_full_name">
                                            Memilih Ucapan Pengantar:
                                        </label>
                                        <select class="form-control select-undangan-ucapan-pengantar" name="id_ucapan_pengantar">
                                            <option value="">Pilih Ucapan Pengantar</option>
                                            @foreach($data as $row)
                                                <option value="{{ $row->id_ucapan_pengantar }}" {{ $row->id_ucapan_pengantar == $order->id_ucapan_pengantar ? 'selected':'' }}>{{ $row->ucp_nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group m-form__group text-center">
                                        <button type="submit" class="btn m-btn--pill btn-success">
                                            <i class="la la-check"></i> Perbarui
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9">
                    <div class="m-portlet m-portlet--mobile akses-list">
                        <div class="m-portlet__body">
                            <h1 class="wrapper-undangan-ucapan-pengantar-judul text-center">{{ $order->ucp_judul }}</h1>
                            <br/>
                            <div class="wrapper-undangan-ucapan-pengantar-isi">{!! $order->ucp_isi !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="hidden">
        @foreach($data as $row)
            <input type="text" name="judul-{{ $row->id_ucapan_pengantar }}" value="{{ $row->ucp_judul }}">
            <textarea name="isi-{{ $row->id_ucapan_pengantar }}">{{ $row->ucp_isi }}</textarea>
        @endforeach
    </div>

@endsection
