@extends('../general/index')

@section('css')

@endsection

@section('js')

@endsection

@section('body')

    <div class="m-grid__item m-grid__item--fluid m-wrapper">
        <div class="m-subheader ">
            <div class="d-flex align-items-center">
                <div class="mr-auto">
                    <h3 class="m-subheader__title text-uppercase m-subheader__title--separator">
                        {{ $pageTitle }}
                    </h3>
                    {!! $breadcrumb !!}
                </div>
            </div>
        </div>

        <div class="m-content">
            <form action="{{ route('mempelaiInsert') }}"
                  enctype="multipart/form-data"
                  method="post"
                  class="m-form form-send"
                  data-alert-show="true"
                  data-alert-field-message="true">

                {{ csrf_field() }}

                <div class="m-portlet m-portlet--mobile akses-list">
                    <div class="m-portlet__body row">
                        <div class="col-sm-12 col-md-6">
                            <h3>1. Mempelai Pria</h3>
                            <br/>
                            <div class="m-form__section m-form__section--first">
                                <div class="form-group m-form__group">
                                    <label class="form-control-label required">
                                        Foto Mempelai Pria
                                    </label>
                                    <br/>
                                    <input type="file" class="m-input">
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="form-control-label required">
                                        Nama Lengkap Pria
                                    </label>
                                    <input type="text" class="form-control m-input" name="pria_opl_nama_mempelai"

                                           autofocus>
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="form-control-label required">
                                        Nama Ayah Pria
                                    </label>
                                    <input type="text" class="form-control m-input" name="pria_opl_nama_ayah" >
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="form-control-label required">
                                        Nama Ibu Pria
                                    </label>
                                    <input type="text" class="form-control m-input" name="pria_opl_nama_ibu" >
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="form-control-label required">
                                        Urutan Anak Pria
                                    </label>
                                    <input type="text" class="form-control m-input" name="pria_opl_urutan_anak"
                                           >
                                </div>
                                <div class="form-group m-form__group">
                                    <label class="form-control-label required">
                                        Alamat Pria
                                    </label>
                                    <input type="text" class="form-control m-input" name="pria_opl_alamat" >
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-6">
                            <h3>2. Mempelai Wanita</h3>
                            <br/>
                            <div class="form-group m-form__group">
                                <label class="form-control-label required">
                                    Foto Mempelai Wanita
                                </label>
                                <br/>
                                <input type="file" class="m-input">
                            </div>
                            <div class="form-group m-form__group">
                                <label class="form-control-label required">
                                    Nama Lengkap Wanita
                                </label>
                                <input type="text" class="form-control m-input" name="wanita_opl_nama_mempelai"

                                       autofocus>
                            </div>
                            <div class="form-group m-form__group">
                                <label class="form-control-label required">
                                    Nama Ayah Wanita
                                </label>
                                <input type="text" class="form-control m-input" name="wanita_opl_nama_ayah" >
                            </div>
                            <div class="form-group m-form__group">
                                <label class="form-control-label required">
                                    Nama Ibu Wanita
                                </label>
                                <input type="text" class="form-control m-input" name="wanita_opl_nama_ibu" >
                            </div>
                            <div class="form-group m-form__group">
                                <label class="form-control-label required">
                                    Urutan Anak Wanita
                                </label>
                                <input type="text" class="form-control m-input" name="wanita_opl_urutan_anak" >
                            </div>
                            <div class="form-group m-form__group">
                                <label class="form-control-label required">
                                    Alamat Wanita
                                </label>
                                <input type="text" class="form-control m-input" name="wanita_opl_alamat" >
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12">
                            <Br />
                            <button type="submit" class="btn btn-success"><i class="la la-check"></i> Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

    </div>
@endsection
