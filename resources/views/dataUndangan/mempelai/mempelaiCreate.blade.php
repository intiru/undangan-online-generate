<form action="{{ route('mempelaiInsert') }}"
      enctype="multipart/form-data"
      method="post"
      class="m-form form-send"
      data-alert-show="true"
      data-alert-field-message="true">

    {{ csrf_field() }}

    <div class="modal" id="modal-create" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="m-portlet__body">
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Kategori Mempelai</label>
                            <select class="form-control m-input" name="id_order_mempelai_kategori">
                                @foreach($mempelai_kategori as $row)
                                    <option value="{{ $row->id_order_mempelai_kategori }}">{{ $row->omk_nama }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <img src="" class="img-thumbnail img-preview" width="150">
                            <br/>
                            <label class="form-control-label">Foto Mempelai</label>
                            <input type="file" class="form-control m-input input-file-browse" name="opl_foto">
                            <span class="m-form__help">Maksimal Upload File Gambar 900KB.</span>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label required">Nama Mempelai</label>
                            <input type="text" class="form-control m-input" name="opl_nama_mempelai">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Jenis Kelamin</label>
                            <select class="form-control" name="opl_jenis_kelamin">
                                <option value="">Kosongkan</option>
                                <option value="putra">Putra</option>
                                <option value="putri">Putri</option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Urutan Anak</label>
                            <select class="form-control" name="opl_urutan_anak">
                                <option value="">Kosongkan</option>
                                <option value="Pertama">Pertama</option>
                                <option value="Kedua">Kedua</option>
                                <option value="Ketiga">Ketiga</option>
                                <option value="Keempat">Keempat</option>
                                <option value="Kelima">Kelima</option>
                                <option value="Keenam">Keenam</option>
                                <option value="Ketujuh">Ketujuh</option>
                                <option value="Kedelapan">Kedelapan</option>
                                <option value="Kesembilan">Kesembilan</option>
                                <option value="Kesepuluh">Kesepuluh</option>
                            </select>
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Nama Ayah</label>
                            <input type="text" class="form-control m-input" name="opl_nama_ayah">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Nama Ibu</label>
                            <input type="text" class="form-control m-input" name="opl_nama_ibu">
                        </div>
                        <div class="form-group m-form__group">
                            <label class="form-control-label">Alamat</label>
                            <textarea class="form-control m-input" name="opl_alamat"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-simpan btn-success">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                </div>
            </div>
        </div>
    </div>
</form>