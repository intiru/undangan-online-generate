<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Session;
use Barryvdh\DomPDF\Facade as PDF;


Route::get('/error-test', function () {
    Log::info('calling the error route');
    throw new \Exception('something unexpected broke');
});

Route::get('/maintenance', function () {
    Artisan::call('down');
});

Route::get('/production', function () {
    Artisan::call('up');
});

Route::post('/submit-form', function () {
    //
})->middleware(\Spatie\HttpLogger\Middlewares\HttpLogger::class);

Route::get('/decrypt/{id}', function($id) {
    return Main::decrypt($id);
});



Route::namespace('Undangan')->group(function() {

//    Route::get('/undangan', "Undangan@index");

    Route::get('/', "Front@index")->name('frontHome');

    Route::get('/i/{username}', "Undangan@index")->name('undanganLink');
    Route::get('/i/{username}/{id_order_tamu_undangan?}', "Undangan@index")->name('undanganLink');
    Route::post('/i/{username}/buku_tamu', "Undangan@buku_tamu")->name('bukuTamuInsert');

    /**
     * Dashboard Order
     */
    Route::get('/dashboard/{username?}/process', "Dashboard@login_process")->name('dashboardOrderProcess');
    Route::get('/dashboard/{username?}', "Dashboard@index")->name('dashboardOrder');

    /**
     * Daftar Buku Tamu
     */
    Route::get('/dashboard/{username?}/buku-tamu', "BukuTamu@index")->name('orderBukuTamuList');

    /**
     * Daftar Tamu Undangan
     */
    Route::get('/dashboard/{username?}/tamu-undangan', "TamuUndangan@index")->name('orderTamuUndanganList');
    Route::post('/dashboard/{username?}/tamu-undangan/insert', "TamuUndangan@insert")->name('orderTamuUndanganInsert');
    Route::delete('/dashboard/{username?}/tamu-undangan/{kode}', "TamuUndangan@delete")->name('orderTamuUndanganDelete');
    Route::get('/dashboard/{username?}/tamu-undangan/modal/{kode}', "TamuUndangan@edit_modal")->name('orderTamuUndanganEditModal');
    Route::post('/dashboard/{username?}/tamu-undangan/{kode}', "TamuUndangan@update")->name('orderTamuUndanganUpdate');

    /**
     * Logout Order
     */
    Route::get('/order/logout', 'Dashboard@logout')->name('logoutOrder');
});

Route::group(['namespace' => 'DataPendukung', 'middleware' => 'authLogin', 'prefix' => 'intiru'], function () {

    /**
     * Backsound
     */
    Route::get('/backsound', 'Backsound@index')->name('backsoundList');
    Route::post('/backsound', "Backsound@insert")->name('backsoundInsert');
    Route::delete('/backsound/{kode}', "Backsound@delete")->name('backsoundDelete');
    Route::get('/backsound/modal/{kode}', "Backsound@edit_modal")->name('backsoundEditModal');
    Route::post('/backsound/{kode}', "Backsound@update")->name('backsoundUpdate');

    /**
     * Protokol Kesehatan
     */
    Route::get('/protokol-kesehatan', 'ProtokolKesehatan@index')->name('protokolKesehatanList');
    Route::post('/protokol-kesehatan', "ProtokolKesehatan@insert")->name('protokolKesehatanInsert');
    Route::delete('/protokol-kesehatan/{kode}', "ProtokolKesehatan@delete")->name('protokolKesehatanDelete');
    Route::get('/protokol-kesehatan/modal/{kode}', "ProtokolKesehatan@edit_modal")->name('protokolKesehatanEditModal');
    Route::post('/protokol-kesehatan/{kode}', "ProtokolKesehatan@update")->name('protokolKesehatanUpdate');

    /**
     * Ucapan Pengantar
     */
    Route::get('/ucapan-pengantar', 'UcapanPengantar@index')->name('ucapanPengantarList');
    Route::post('/ucapan-pengantar', "UcapanPengantar@insert")->name('ucapanPengantarInsert');
    Route::delete('/ucapan-pengantar/{kode}', "UcapanPengantar@delete")->name('ucapanPengantarDelete');
    Route::get('/ucapan-pengantar/modal/{kode}', "UcapanPengantar@edit_modal")->name('ucapanPengantarEditModal');
    Route::post('/ucapan-pengantar/{kode}', "UcapanPengantar@update")->name('ucapanPengantarUpdate');

    /**
     * Ucapan Penutup
     */
    Route::get('/ucapan-penutup', 'UcapanPenutup@index')->name('ucapanPenutupList');
    Route::post('/ucapan-penutup', "UcapanPenutup@insert")->name('ucapanPenutupInsert');
    Route::delete('/ucapan-penutup/{kode}', "UcapanPenutup@delete")->name('ucapanPenutupDelete');
    Route::get('/ucapan-penutup/modal/{kode}', "UcapanPenutup@edit_modal")->name('ucapanPenutupEditModal');
    Route::post('/ucapan-penutup/{kode}', "UcapanPenutup@update")->name('ucapanPenutupUpdate');

    /**
     * Mantra Penutup
     */
    Route::get('/mantra-penutup', 'MantraPenutup@index')->name('mantraPenutupList');
    Route::post('/mantra-penutup', "MantraPenutup@insert")->name('mantraPenutupInsert');
    Route::delete('/mantra-penutup/{kode}', "MantraPenutup@delete")->name('mantraPenutupDelete');
    Route::get('/mantra-penutup/modal/{kode}', "MantraPenutup@edit_modal")->name('mantraPenutupEditModal');
    Route::post('/mantra-penutup/{kode}', "MantraPenutup@update")->name('mantraPenutupUpdate');

    /**
     * Message Chat
     */
    Route::get('/message-chat', 'MessageChat@index')->name('messageChatList');
    Route::post('/message-chat', "MessageChat@insert")->name('messageChatInsert');
    Route::delete('/message-chat/{kode}', "MessageChat@delete")->name('messageChatDelete');
    Route::get('/message-chat/modal/{kode}', "MessageChat@edit_modal")->name('messageChatEditModal');
    Route::post('/message-chat/{kode}', "MessageChat@update")->name('messageChatUpdate');

});


Route::group(['namespace' => 'DataUndangan', 'middleware' => 'authLogin', 'prefix' => 'intiru'], function () {

    /**
     * Data Undangan
     */
    Route::get('/data-undangan', 'DataUndangan@index')->name('dataUndanganList');
    Route::post('/data-undangan', "DataUndangan@insert")->name('dataUndanganInsert');
    Route::delete('/data-undangan/{id}', "DataUndangan@delete")->name('dataUndanganDelete');
    Route::get('/data-undangan/modal/{kode}', "DataUndangan@edit_modal")->name('dataUndanganEditModal');
    Route::post('/data-undangan/{kode}', "DataUndangan@update")->name('dataUndanganUpdate');
    Route::post('/data-undangan/status-aktif/{id}', "DataUndangan@order_aktif")->name('dataUndanganOrderAktif');
    Route::post('/data-undangan/backsound/{id}', "DataUndangan@backsound_update")->name('dataUndanganBacksoundUpdate');

    Route::get('/data-undangan/order-process/{id}', "DataUndangan@order_process")->name('dataUndanganProcess');
    Route::get('/data-undangan/menu', "DataUndangan@menu")->name('dataUndanganMenu');

    /**
     * Mempelai
     */
    Route::get('/data-undangan/mempelai', "Mempelai@index")->name('mempelaiList');
    Route::post('/data-undangan/mempelai/insert', "Mempelai@insert")->name('mempelaiInsert');
    Route::delete('/data-undangan/mempelai/{kode}', "Mempelai@delete")->name('mempelaiDelete');
    Route::get('/data-undangan/mempelai/modal/{kode}', "Mempelai@edit_modal")->name('mempelaiEditModal');
    Route::post('/data-undangan/mempelai/{kode}', "Mempelai@update")->name('mempelaiUpdate');


    /**
     * Kategori Mempelai
     */
    Route::get('/data-undangan/mempelai-kategori/list', 'MempelaiKategori@index')->name('mempelaiKategoriList');
    Route::post('/data-undangan/mempelai-kategori/insert', "MempelaiKategori@insert")->name('mempelaiKategoriInsert');
    Route::delete('/data-undangan/mempelai-kategori/delete/{kode}', "MempelaiKategori@delete")->name('mempelaiKategoriDelete');
    Route::get('/data-undangan/mempelai-kategori/edit/modal/{kode}', "MempelaiKategori@edit_modal")->name('mempelaiKategoriEditModal');
    Route::post('/data-undangan/mempelai-kategori/update/{kode}', "MempelaiKategori@update")->name('mempelaiKategoriUpdate');


    /**
     * Ucapan Pengantar
     */
    Route::get('/data-undangan/ucapan-pengantar', "UcapanPengantar@index")->name('undanganUcapanPengantarList');
    Route::post('/data-undangan/ucapan-pengantar/update', "UcapanPengantar@update")->name('undanganUcapanPengantarUpdate');

    /**
     * Lokasi & Waktu
     */
    Route::get('/data-undangan/lokasi-waktu', "LokasiWaktu@index")->name('undanganLokasiWaktuList');
    Route::post('/data-undangan/lokasi-waktu/update', "LokasiWaktu@update")->name('undanganLokasiWaktuUpdate');

    /**
     * Cover Foto Undangan
     */
//    Route::get('/data-undangan/cover-undangan', "CoverUndangan@index")->name('undanganCoverList');
//    Route::post('/data-undangan/cover-undangan/update', "CoverUndangan@update")->name('undanganCoverUpdate');


    Route::get('/data-undangan/cover-undangan', 'CoverUndangan@index')->name('coverUndanganList');
    Route::post('/data-undangan/cover-undangan/insert', "CoverUndangan@insert")->name('coverUndanganInsert');
    Route::delete('/data-undangan/cover-undangan/{kode}', "CoverUndangan@delete")->name('coverUndanganDelete');
    Route::get('/data-undangan/cover-undangan/modal/{kode}', "CoverUndangan@edit_modal")->name('coverUndanganEditModal');
    Route::post('/data-undangan/cover-undangan/update/{kode}', "CoverUndangan@update")->name('coverUndanganUpdate');

    /**
     * Galeri Foto Undangan
     */
    Route::get('/data-undangan/galeri-undangan', 'GaleriUndangan@index')->name('undanganGaleriList');
    Route::post('/data-undangan/galeri-undangan/insert', "GaleriUndangan@insert")->name('undanganGaleriInsert');
    Route::delete('/data-undangan/galeri-undangan/{kode}', "GaleriUndangan@delete")->name('undanganGaleriDelete');
    Route::get('/data-undangan/galeri-undangan/modal/{kode}', "GaleriUndangan@edit_modal")->name('undanganGaleriEditModal');
    Route::post('/data-undangan/galeri-undangan/{kode}', "GaleriUndangan@update")->name('undanganGaleriUpdate');

    /**
     * Thumbnail Undangan
     */
    Route::get('/data-undangan/thumbnail', 'ThumbnailUndangan@index')->name('undanganThumbnailList');
    Route::post('/data-undangan/thumbnail/update', "ThumbnailUndangan@update")->name('undanganThumbnailUpdate');


    /**
     * Video Undangan
     */
    // old
//    Route::get('/data-undangan/video-undangan', "VideoUndangan@index")->name('undanganVideoList');
//    Route::post('/data-undangan/video-undangan/update', "VideoUndangan@update")->name('undanganVideoUpdate');

    Route::get('/data-undangan/video-undangan', 'VideoUndangan@index')->name('undanganVideoList');
    Route::post('/data-undangan/video-undangan/insert', "VideoUndangan@insert")->name('undanganVideoInsert');
    Route::delete('/data-undangan/video-undangan/delete/{kode}', "VideoUndangan@delete")->name('undanganVideoDelete');
    Route::get('/data-undangan/video-undangan/edit/modal/{kode}', "VideoUndangan@edit_modal")->name('undanganVideoEditModal');
    Route::post('/data-undangan/video-undangan/update/{kode}', "VideoUndangan@update")->name('undanganVideoUpdate');

    /**
     * Musik Undangan
     */
    Route::get('/data-undangan/musik-undangan', "MusikUndangan@index")->name('undanganMusikList');

    /**
     * Ucapan Penutup
     */
    Route::get('/data-undangan/ucapan-penutup', "UcapanPenutup@index")->name('undanganUcapanPenutupList');
    Route::post('/data-undangan/ucapan-penutup/update', "UcapanPenutup@update")->name('undanganUcapanPenutupUpdate');

    /**
     * Mantra Penutup
     */
    Route::get('/data-undangan/mantra-penutup', "MantraPenutup@index")->name('undanganMantraPenutupList');
    Route::post('/data-undangan/mantra-penutup/update', "MantraPenutup@update")->name('undanganMantraPenutupUpdate');


    /**
     * Daftar Buku Tamu
     */
    Route::get('/data-undangan/buku-tamu', "BukuTamu@index")->name('undanganBukuTamuList');

    /**
     * Daftar Tamu Undangan
     */
    Route::get('/data-undangan/tamu-undangan', "TamuUndangan@index")->name('undanganTamuUndanganList');
    Route::post('/data-undangan/tamu-undangan/insert', "TamuUndangan@insert")->name('undanganTamuUndanganInsert');
    Route::delete('/data-undangan/tamu-undangan/{kode}', "TamuUndangan@delete")->name('undanganTamuUndanganDelete');
    Route::get('/data-undangan/tamu-undangan/modal/{kode}', "TamuUndangan@edit_modal")->name('undanganTamuUndanganEditModal');
    Route::post('/data-undangan/tamu-undangan/{kode}', "TamuUndangan@update")->name('undanganTamuUndanganUpdate');


});



Route::namespace('General')->group(function () {

    Route::group(['prefix' => 'intiru'], function() {

        Route::get('undercosntruction', "Underconstruction@index")->name('underconstructionPage');
        Route::post('city/list', "General@city_list")->name('cityList');
        Route::post('subdistrict/list', "General@subdistrict_list")->name('subdistrictList');

        Route::group(['middleware' => 'checkLogin'], function () {
            Route::get('/', "Login@index")->name("loginPage");
            Route::post('/roles/login', 'Login@roles')->name("rolesLogin");
            Route::post('/login', "Login@do")->name("loginDo");
        });

        Route::group(['middleware' => 'authLogin'], function () {
            Route::get('/dashboard', "Dashboard@index")->name('dashboardPage');
            Route::get('/whatsapp-test', "Dashboard@whatsapp_test")->name('whatsappTest');
            Route::get('/logout', "Logout@do")->name('logoutDo');
            Route::get('/profile', "Profile@index")->name('profilPage');
            Route::post('/profile/administrator', "Profile@update_administrator")->name('profilAdministratorUpdate');

        });

    });

    Route::get('/', function() {
        return 'Halaman Pengenalan Website DWZ';
    });
});


// =============== Master Data =================== //

Route::group(['namespace' => 'MasterData', 'middleware' => 'authLogin'], function () {

    // Karyawan
    Route::get('/karyawan', "Karyawan@index")->name('karyawanPage');
    Route::post('/karyawan', "Karyawan@insert")->name('karyawanInsert');
    Route::get('/karyawan-list', "Karyawan@list")->name('karyawanList');
    Route::delete('/karyawan/{id}', "Karyawan@delete")->name('karyawanDelete');
    Route::get('/karyawan/{id}', "Karyawan@edit_modal")->name('karyawanEditModal');
    Route::post('/karyawan/{id}', "Karyawan@update")->name('karyawanUpdate');

    // User
    Route::get('/user', "User@index")->name('userPage');
    Route::post('/user', "User@insert")->name('userInsert');
    Route::get('/user-list', "User@list")->name('userList');
    Route::delete('/user/{kode}', "User@delete")->name('userDelete');
    Route::get('/user/modal/{kode}', "User@edit_modal")->name('userEditModal');
    Route::post('/user/{kode}', "User@update")->name('userUpdate');

    // Role User
    Route::get('/user-role', "UserRole@index")->name('userRolePage');
    Route::post('/user-role', "UserRole@insert")->name('userRoleInsert');
    Route::delete('/user-role/{id}', "UserRole@delete")->name('userRoleDelete');
    Route::get('/user-role/modal/{id}', "UserRole@edit")->name('userRoleEditModal');
    Route::post('/user-role/{id}', "UserRole@update")->name('userRoleUpdate');
    Route::get('/user-role/akses/{id}', "UserRole@akses")->name('userRoleAkses');
    Route::post('/user-role/akses/{id}', "UserRole@akses_update")->name('userRoleAksesUpdate');

});

Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    //Artisan::call('route:cache');
    Artisan::call('config:cache');
    Artisan::call('view:clear');
    return "Cache is cleared";
});

